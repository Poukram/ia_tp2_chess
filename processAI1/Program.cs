﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Threading;

namespace processAI1
{
    class Program
    {
        public bool stop;
        private Agent mayhem;
        private string rep;
        private string mutex_start;
        private string mutex_ai;

        public Program()
        {
            mayhem = new Agent(this);
            stop = false;
            rep = "repAI1";
            mutex_start = "mutexStartAI1";
            mutex_ai = "mutexAI1";

            Console.WriteLine("What color am I? b for black, w for white\n\t");
            char color = (char) Console.Read();

            if (color == 'b')
            {
                rep = "repAI2";
                mutex_start = "mutexStartAI2";
                mutex_ai = "mutexAI2";
            }
        }

        public void Run()
        {
            try
            {
                while (!stop)
                {
                    using (var mmf = MemoryMappedFile.OpenExisting("plateau"))
                    {
                        using (var mmf2 = MemoryMappedFile.OpenExisting(rep))
                        {
                            Mutex mutexStart = Mutex.OpenExisting(mutex_start);
                            Mutex mutex = Mutex.OpenExisting(mutex_ai);
                            mutex.WaitOne();

                            mutexStart.WaitOne();

                            mayhem.Observe(mmf.CreateViewAccessor());

                            if (!stop)
                            {
                                /******************************************************************************************************/
                                /***************************************** ECRIRE LE CODE DE L'IA *************************************/
                                /******************************************************************************************************/

                                mayhem.Update();
                                mayhem.Search();

                                /********************************************************************************************************/
                                /********************************************************************************************************/
                                /********************************************************************************************************/

                                mayhem.JustDoIt(mmf2.CreateViewAccessor());
                            }
                            mutex.ReleaseMutex();
                            mutexStart.ReleaseMutex();
                        }
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Memory-mapped file does not exist. Run Process A first.");
                Console.ReadLine();
            }
        }

        private static void TestPerft()
        {
            Perft p = new Perft();
            for (int i = 1; i < 7; i++)
            {
                p.RunConfigurationAtDepth(i, 1);
                p.RunConfigurationAtDepth(i, 2);
                p.RunConfigurationAtDepth(i, 3);
                p.RunConfigurationAtDepth(i, 4);
                Console.ReadKey();
            }
        }

        private static void TestBrain()
        {
            ArrayBoard b = new ArrayBoard();
            b.SetBoardDefault();
            Brain brain = new Brain(b);
            brain.Test();
            Console.Write(b.GameMovesToString());
            Console.ReadKey();
        }

        static void Main()
        {
            Console.WriteLine("(INITIALIZATION)");
            HashNode hn = new HashNode();
            bool start = Utils.IsHit(0, ref hn);

            Program p = new Program();
            p.Run();
            Console.Write(p.mayhem.board.GameMovesToString());
            Console.ReadKey();
        }
    }
}
