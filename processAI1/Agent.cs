﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Threading;



namespace processAI1
{
    class Agent
    {
        private int[] tabVal;
        private String value;
        private Program p;
        private RootMove best_sofar;
        public String[] formated_best_mv;
        public ArrayBoard board;
        public Brain brain;

        public Agent(Program p)
        {
            this.p = p;
            this.tabVal = new int[64];
            formated_best_mv = new String[] { "", "", "" };
            board = new ArrayBoard();
            brain = new Brain(board);

            Console.WriteLine("(INITIALIZATION DONE)");
        }

        // Receive information from the game aka environment
        public void Observe(MemoryMappedViewAccessor accessor)
        {
            ushort Size = accessor.ReadUInt16(0);
            byte[] Buffer = new byte[Size];
            accessor.ReadArray(0 + 2, Buffer, 0, Buffer.Length);

            value = ASCIIEncoding.ASCII.GetString(Buffer);
            if (value == "stop")
                p.stop = true;
            else
            {
                String[] substrings = value.Split(',');
                for (int i = 0; i < substrings.Length; i++)
                {
                    tabVal[i] = Convert.ToInt32(substrings[i]);
                }
            }
        }

        private void FindLastMoveAndPlayIt()
        {
            Stopwatch sw = new Stopwatch();
            sw.Restart();

            sbyte[] new_b = board.GenBoard(tabVal);
            UInt64 new_h = Utils.ComputeHash(new_b);
            bool found = true;

            if (new_h == board.hash)
                return;

            SortedSet<Move> lm = new SortedSet<Move>();
            brain.mg.GenAllLegalMoves(lm);


            foreach(Move m in lm)
            {
                if (brain.mg.MakeMove(m) == Utils.LEGAL)
                {
                    found = true;
                    for (int i = 0; i < 64; i++)
                    {
                        if (new_b[i] != board.board[i])
                        {
                            found = false;
                            break;
                        }
                    }
                    if (!found)
                        brain.mg.UnmakeLastMove();
                    else
                        break;
                }
            }

            // Dirty hack
            if (!found)
            {
                Console.WriteLine("Had to use the dirty hack to update board..");
                board.board = new_b;
                board.plys_since_game_start++;
                board.side_to_move *= -1;
            }
            Console.WriteLine("Took " + sw.ElapsedMilliseconds + "ms to find opponent's move");
        }


        public void Update()
        {
            FindLastMoveAndPlayIt();

            Console.Write(board.ToString());
        }

        public void Search()
        {
            best_sofar = brain.IterativeDepthFindBestMoveWithDepth(6);
            Console.WriteLine("\nFound best move : " + best_sofar.m.ToString() + " score : "
                + best_sofar.score + " in : " + best_sofar.time + "ms");
            brain.mg.MakeMove(best_sofar.m);
            formated_best_mv = Utils.ConvertMoveForPlatform(best_sofar.m);
        }

        public void JustDoIt(MemoryMappedViewAccessor accessor)
        {
            value = "";
            value = formated_best_mv[0];
            for (int i = 1; i < formated_best_mv.Length; i++)
            {
                value += "," + formated_best_mv[i];
            }
            byte[] Buffer = ASCIIEncoding.ASCII.GetBytes(value);
            accessor.Write(0, (ushort)Buffer.Length);
            accessor.WriteArray(0 + 2, Buffer, 0, Buffer.Length);
        }
    }
}
