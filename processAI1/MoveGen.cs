﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace processAI1
{
    class MoveGen
    {
        private ArrayBoard b;

        public MoveGen(ArrayBoard b)
        {
            this.b = b;
        }

        public void GenAllLegalMoves(SortedSet<Move> lm)
        {
            sbyte cur_piece = -127;
            sbyte actual_piece = -127;
            sbyte found_sq_idx = -127;
            sbyte found_piece = -127;

            for (byte curr_sq_idx = 0; curr_sq_idx < 64; curr_sq_idx++)
            {
                cur_piece = b.board[curr_sq_idx];
                actual_piece = (sbyte) (cur_piece * b.side_to_move);

                // Simple sign rule as white is + and black - if its > 0 then it belongs to STM
                if (cur_piece != Utils.EMPTY && actual_piece > 0)
                {
                    if (actual_piece == Utils.PAWN)
                    {
                        sbyte s = -1;
                        sbyte n = -1;
                        sbyte sw = -1;
                        sbyte se = -1;
                        sbyte nw = -1;
                        sbyte ne = -1;

                        if (((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_SOUTH + Utils.x88_WEST) & 0x88) == 0)
                            sw = (sbyte)(curr_sq_idx + Utils.SOUTH + Utils.WEST);
                        if (((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_SOUTH + Utils.x88_EAST) & 0x88) == 0)
                            se = (sbyte)(curr_sq_idx + Utils.SOUTH + Utils.EAST);
                        if (((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_NORTH + Utils.x88_WEST) & 0x88) == 0)
                            nw = (sbyte)(curr_sq_idx + Utils.NORTH + Utils.WEST);
                        if (((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_NORTH + Utils.x88_EAST) & 0x88) == 0)
                            ne = (sbyte)(curr_sq_idx + Utils.NORTH + Utils.EAST);
                        if (((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_SOUTH) & 0x88) == 0)
                            s = (sbyte)(curr_sq_idx + Utils.SOUTH);
                        if (((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_NORTH) & 0x88) == 0)
                            n = (sbyte)(curr_sq_idx + Utils.NORTH);

                        // Pawns handled apart because they only move in one direction
                        if (b.side_to_move == Utils.WHITE)
                        {
                            if (nw != -1 && b.board[nw] * Utils.BLACK > 0 && b.board[nw] != Utils.EMPTY)
                                GenMoveTypeFromTo(Utils.PAWN_MVE + Utils.CAPTURE, curr_sq_idx, (byte)nw, lm);

                            if (ne != -1 && b.board[ne] * Utils.BLACK > 0 && b.board[ne] != Utils.EMPTY)
                                GenMoveTypeFromTo(Utils.PAWN_MVE + Utils.CAPTURE, curr_sq_idx, (byte)ne, lm);

                            if (n != -1 && b.board[n] == Utils.EMPTY)
                            {
                                GenMoveTypeFromTo(Utils.PAWN_MVE, curr_sq_idx, (byte)n, lm);
                                if (Utils.LOOKUP_ROW[curr_sq_idx] == 6 && b.board[n + Utils.NORTH] == Utils.EMPTY)
                                    GenMoveTypeFromTo(Utils.PUSH_PAW + Utils.PAWN_MVE, curr_sq_idx, (byte)(n + Utils.NORTH), lm);
                            }
                        }
                        else
                        {
                            if (sw != -1 && b.board[sw] * Utils.WHITE > 0 && b.board[sw] != Utils.EMPTY)
                                GenMoveTypeFromTo(Utils.PAWN_MVE + Utils.CAPTURE, curr_sq_idx, (byte)sw, lm);

                            if (se != -1 && b.board[se] * Utils.WHITE > 0 && b.board[se] != Utils.EMPTY)
                                GenMoveTypeFromTo(Utils.PAWN_MVE + Utils.CAPTURE, curr_sq_idx, (byte)se, lm);

                            if (s != -1 && b.board[s] == Utils.EMPTY)
                            {
                                GenMoveTypeFromTo(Utils.PAWN_MVE, curr_sq_idx, (byte)s, lm);
                                if (Utils.LOOKUP_ROW[curr_sq_idx] == 1 && b.board[s + Utils.SOUTH] == Utils.EMPTY)
                                    GenMoveTypeFromTo(Utils.PUSH_PAW + Utils.PAWN_MVE, curr_sq_idx, (byte)(s + Utils.SOUTH), lm);
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0; j < Utils.PIECE_NUM_DIR[actual_piece]; j++)
                        {
                            sbyte tmp = Utils.LOOKUP_0x88_INV[curr_sq_idx];
                            // This while is for sliding pieces
                            while (true) {
                                tmp += Utils.x88_PIECE_DIR[actual_piece, j];
                                if ((tmp & 0x88) != 0)
                                    break;
                                // return to 8x8 board now that we know square is valid
                                found_sq_idx = Utils.LOOKUP_0x88[tmp];
                                found_piece = b.board[found_sq_idx];

                                if (found_piece != Utils.EMPTY)
                                {
                                    if ((found_piece * b.side_to_move) < 0) 
                                        GenMoveTypeFromTo(Utils.CAPTURE, curr_sq_idx, (byte)found_sq_idx, lm);
                                    break; // break because we won't slide past one of our own or after having captured 
                                }
                                // register that move on an empty square
                                if (found_piece == Utils.EMPTY)
                                    GenMoveTypeFromTo(Utils.NORMAL_MVE, curr_sq_idx, (byte)found_sq_idx, lm);

                                //break after the first step if not a sliding piece
                                if (Utils.IS_SLIDING[actual_piece] == 0)
                                    break;
                            }
                        }
                    }
                }
            }

            // Deal with castling
            if (b.side_to_move == Utils.WHITE)
            {
                if (b.castling_rights.Get(3))
                    GenMoveTypeFromTo(Utils.CASTLE + Utils.KS_CASTLE, 60, 62, lm); // E1 G1
                if (b.castling_rights.Get(2))
                    GenMoveTypeFromTo(Utils.CASTLE + Utils.QS_CASTLE, 60, 58, lm); // E1 C1
            }
            else
            {
                if (b.castling_rights.Get(1))
                    GenMoveTypeFromTo(Utils.CASTLE + Utils.KS_CASTLE, 4, 6, lm); // E8 G8
                if (b.castling_rights.Get(0))
                    GenMoveTypeFromTo(Utils.CASTLE + Utils.QS_CASTLE, 4, 2, lm); // E8 C8
            }

            // Handling of the en passant case
            if (b.enpassant_idx != -1)
            {
                byte ep = (byte) b.enpassant_idx;
                sbyte sw_from_ep = -1;
                sbyte se_from_ep = -1;
                sbyte nw_from_ep = -1;
                sbyte ne_from_ep = -1;

                if (((Utils.LOOKUP_0x88_INV[ep] + Utils.x88_SOUTH + Utils.x88_WEST) & 0x88) == 0)
                    sw_from_ep = (sbyte)(ep + Utils.SOUTH + Utils.WEST);
                if (((Utils.LOOKUP_0x88_INV[ep] + Utils.x88_SOUTH + Utils.x88_EAST) & 0x88) == 0)
                    se_from_ep = (sbyte)(ep + Utils.SOUTH + Utils.EAST);
                if (((Utils.LOOKUP_0x88_INV[ep] + Utils.x88_NORTH + Utils.x88_WEST) & 0x88) == 0)
                    nw_from_ep = (sbyte)(ep + Utils.NORTH + Utils.WEST);
                if (((Utils.LOOKUP_0x88_INV[ep] + Utils.x88_NORTH + Utils.x88_EAST) & 0x88) == 0)
                    ne_from_ep = (sbyte)(ep + Utils.NORTH + Utils.EAST);

                if (b.side_to_move == Utils.WHITE)
                {
                    if (sw_from_ep != -1 && b.board[sw_from_ep] == Utils.PAWN * Utils.WHITE)
                        GenMoveTypeFromTo(Utils.ENPASS + Utils.PAWN_MVE + Utils.CAPTURE, (byte)sw_from_ep, ep, lm);
                    if (se_from_ep != -1 && b.board[se_from_ep] == Utils.PAWN * Utils.WHITE)
                        GenMoveTypeFromTo(Utils.ENPASS + Utils.PAWN_MVE + Utils.CAPTURE, (byte)se_from_ep, ep, lm);
                }
                else
                {
                    if (nw_from_ep != -1 && b.board[nw_from_ep] == Utils.PAWN * Utils.BLACK)
                        GenMoveTypeFromTo(Utils.ENPASS + Utils.PAWN_MVE + Utils.CAPTURE, (byte)nw_from_ep, ep, lm);
                    if (ne_from_ep != -1 && b.board[ne_from_ep] == Utils.PAWN * Utils.BLACK)
                        GenMoveTypeFromTo(Utils.ENPASS + Utils.PAWN_MVE + Utils.CAPTURE, (byte)ne_from_ep, ep, lm);
                }
            }
        }

        private void GenMoveTypeFromTo(int move_type, byte from, byte to, SortedSet<Move> lm)
        {

            if ((move_type & Utils.PAWN_MVE) != 0)
            {
                if ((b.side_to_move == Utils.WHITE && Utils.LOOKUP_ROW[to] == 0) || (
                    b.side_to_move == Utils.BLACK && Utils.LOOKUP_ROW[to] == 7))
                {
                    // use special promotion generator
                    GenPromotionTypeFromTo(move_type, from, to, lm);
                    return;
                }
            }

            Move m = new Move()
            {
                to = (byte)to,
                fm = (byte)from,
                promotion = 0,
                move_type = (byte)move_type,
                score = 0
            };

            sbyte to_piece = (sbyte)(b.board[to] * -1 * b.side_to_move);
            sbyte from_piece = (sbyte)(b.board[from] * b.side_to_move);

            // Most valuable victim and less valuable attacker
            if (b.board[m.to] != Utils.EMPTY && (m.move_type & Utils.CAPTURE) != 0)
            {
                if (to_piece == from_piece && to_piece == Utils.KING)
                    m.score = 0;
                else
                    m.score = Utils.CAPTURE_BONUS + (Utils.PIECE_VALUE[to_piece] * 20) - Utils.PIECE_VALUE[from_piece];
            }
            else
            {
                if (b.side_to_move == Utils.WHITE)
                    m.score = b.history_h[0, m.fm, m.to];
                else
                    m.score = b.history_h[1, m.fm, m.to];
            }

            // BEGIN EXPERIMENTAL!!!!!!
            int dist = 0;
            if (b.side_to_move == Utils.WHITE)
                dist = (Utils.LOOKUP_SQ_REL[Utils.LOOKUP_SQ_REL_IDX_FROM_TO[from, b.black_king_idx]] & 7);
            else
                dist = (Utils.LOOKUP_SQ_REL[Utils.LOOKUP_SQ_REL_IDX_FROM_TO[from, b.white_king_idx]] & 7);

            if (dist <= 2 &&  from_piece != Utils.KING)
                m.score += (3 - dist) * Utils.ENEMY_KING_PROXIMITY_BONUS;
            // END EXPERIMENTAL!!!!!!


            if (b.history.Count > 0 && m.Equals(b.counter_h[b.history.ElementAt(b.history.Count - 1).m.fm, b.history.Last().m.to]))
                m.score += Utils.COUNTER_MOVE_BONUS;
            
            lm.Add(m);
        }

        private static void GenPromotionTypeFromTo(int move_type, byte from, byte to, SortedSet<Move> lm)
        {
            for (byte i = Utils.KNIGHT; i <= Utils.QUEEN; i++)
            {
                Move m = new Move()
                {
                    to = (byte)to,
                    fm = (byte)from,
                    promotion = i,
                    move_type = (byte)(move_type | 32),
                    score = Utils.CAPTURE_BONUS + (Utils.PIECE_VALUE[i] * 10)
                };

                lm.Add(m);
            }

        }

        // Returns how many attacking pieces there is
        private byte IsAttacked(byte sq_idx, sbyte attacker)
        {
            bool dir_found = false;
            byte curr_sq_rel = 255;
            byte curr_mv_type = 16;
            byte curr_dist = 7;
            sbyte curr_piece = (sbyte) Utils.EMPTY;
            sbyte curr_dir = -127;
            byte attacking_pieces = 0;

            // Lets run over each square and see if one can be a threat
            for (byte curr_sq_idx = 0; curr_sq_idx < 64; curr_sq_idx++)
            {
                curr_piece = (sbyte)(b.board[curr_sq_idx] * attacker);

                // if its empty or not our own or the looked up square we don't care
                if (curr_sq_idx == sq_idx || curr_piece == Utils.EMPTY || curr_piece < 0)
                    continue;

                curr_sq_rel = Utils.LOOKUP_SQ_REL[Utils.LOOKUP_SQ_REL_IDX_FROM_TO[curr_sq_idx, sq_idx]];
                curr_mv_type = (byte)(curr_sq_rel >> 3);
                curr_dist = (byte)(curr_sq_rel & 7);

                if ((curr_mv_type & Utils.SQ_REL_INVALID_MV) != 0)
                    continue;

                if (curr_mv_type > 7)
                {
                    if (curr_piece == Utils.KNIGHT)
                        attacking_pieces++;
                    continue;
                }

                if ((curr_piece == Utils.PAWN && curr_dist > 1) || (Utils.IS_SLIDING[curr_piece] == 0 && curr_dist > 1))
                    continue;

                dir_found = false;
                curr_dir = Utils.x88_SQ_REL_MV_DIR[curr_mv_type];

                for (int k = 0; k < Utils.PIECE_NUM_DIR[curr_piece]; k++)
                {
                    if (Utils.x88_PIECE_DIR[curr_piece, k] == curr_dir)
                    {
                        dir_found = true;
                        break;
                    }
                }

                if (dir_found)
                {
                    int tmp_idx = curr_sq_idx;
                    bool path_clear = true;


                    for (int j = 0; j < curr_dist; j++)
                    {
                        if (((Utils.LOOKUP_0x88_INV[tmp_idx] + curr_dir) & 0x88) == 0)
                            tmp_idx = Utils.LOOKUP_0x88[Utils.LOOKUP_0x88_INV[tmp_idx] + curr_dir];
                        else
                        {
                            path_clear = false;
                            break;
                        }

                        if (b.board[tmp_idx] != Utils.EMPTY && tmp_idx != sq_idx)
                        {
                            path_clear = false;
                            break;
                        }
                    }

                    if (path_clear)
                        attacking_pieces++;
                    continue;
                }

                if (curr_piece == Utils.PAWN)
                {
                    if (attacker == Utils.WHITE)
                    {
                        if ((Utils.LOOKUP_COL[curr_sq_idx] != 0 &&
                            ((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_NORTH + Utils.x88_WEST) & 0x88) == 0 &&
                            curr_sq_idx + Utils.NORTH + Utils.WEST == sq_idx) ||
                            (Utils.LOOKUP_COL[curr_sq_idx] != 7 &&
                            ((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_NORTH + Utils.x88_EAST) & 0x88) == 0 &&
                            curr_sq_idx + Utils.NORTH + Utils.EAST == sq_idx))
                            attacking_pieces++;
                    }
                    else
                    {
                        if ((Utils.LOOKUP_COL[curr_sq_idx] != 0 &&
                            ((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_SOUTH + Utils.x88_WEST) & 0x88) == 0 &&
                            curr_sq_idx + Utils.SOUTH + Utils.WEST == sq_idx) ||
                            (Utils.LOOKUP_COL[curr_sq_idx] != 7 &&
                            ((Utils.LOOKUP_0x88_INV[curr_sq_idx] + Utils.x88_SOUTH + Utils.x88_EAST) & 0x88) == 0 &&
                            curr_sq_idx + Utils.SOUTH + Utils.EAST == sq_idx))
                            attacking_pieces++;
                    }

                }

            }
            return attacking_pieces;
        }

        // Returns how many attacking pieces there is
        public byte IsInCheck(sbyte attacker)
        {
            if (attacker == Utils.WHITE)
                return IsAttacked(b.black_king_idx, attacker);
            else
                return IsAttacked(b.white_king_idx, attacker);
        }

        public byte MakeMove(Move m)
        {
            BitArray tmp_cr = new BitArray(b.castling_rights);

            // We verify is castling is legal
            if ((m.move_type & Utils.CASTLE) != 0)
            {
                byte rook_from_idx = 255;
                byte rook_to_idx = 255;

                // first see if king is in check before going further
                if (IsInCheck((sbyte) (b.side_to_move * -1)) > 0)
                    return Utils.IN_CHECK;
                if (b.side_to_move == Utils.WHITE)
                {
                    if ((m.move_type & Utils.KS_CASTLE) != 0) { 
                        if (b.board[61] != Utils.EMPTY || b.board[62] != Utils.EMPTY ||
                            IsAttacked(61, (sbyte)(b.side_to_move * -1)) > 0 || IsAttacked(62, (sbyte)(b.side_to_move * -1)) > 0)
                            return Utils.ILLEGAL_CASTLE;
                        rook_from_idx = 63;
                        rook_to_idx = 61;
                        tmp_cr.Set(3, false);
                    }
                    else if ((m.move_type & Utils.QS_CASTLE) != 0){
                        if (b.board[57] != Utils.EMPTY || b.board[58] != Utils.EMPTY || b.board[59] != Utils.EMPTY ||
                            IsAttacked(58, (sbyte)(b.side_to_move * -1)) > 0 || IsAttacked(59, (sbyte)(b.side_to_move * -1)) > 0)
                            return Utils.ILLEGAL_CASTLE;
                        rook_from_idx = 56;
                        rook_to_idx = 59;
                        tmp_cr.Set(2, false);
                    }
                }
                else
                {
                    if ((m.move_type & Utils.KS_CASTLE) != 0) { 
                        if (b.board[5] != Utils.EMPTY || b.board[6] != Utils.EMPTY ||
                            IsAttacked(5, (sbyte)(b.side_to_move * -1)) > 0 || IsAttacked(6, (sbyte)(b.side_to_move * -1)) > 0)
                            return Utils.ILLEGAL_CASTLE;
                        rook_from_idx = 7;
                        rook_to_idx = 5;
                        tmp_cr.Set(1, false);
                    }
                    else if ((m.move_type & Utils.QS_CASTLE) != 0){
                        if (b.board[1] != Utils.EMPTY || b.board[2] != Utils.EMPTY || b.board[3] != Utils.EMPTY ||
                            IsAttacked(2, (sbyte)(b.side_to_move * -1)) > 0 || IsAttacked(3, (sbyte)(b.side_to_move * -1)) > 0)
                            return Utils.ILLEGAL_CASTLE;
                        rook_from_idx = 0;
                        rook_to_idx = 3;
                        tmp_cr.Set(0, false);
                    }
                }
                b.board[rook_to_idx] = b.board[rook_from_idx];
                b.board[rook_from_idx] = (sbyte) Utils.EMPTY;

                byte piece = Utils.ROOK - 1;
                if (b.side_to_move == Utils.BLACK)
                    piece += 6;
            }

            // Store info about the move in a history structure to undo it later
            History h = new History()
            {
                m = m,
                captured_piece = b.board[m.to],
                enpassant_idx = b.enpassant_idx,
                castling_rights = new BitArray(b.castling_rights),
                fifty_mve_rule = b.fifty_mve_rule,
                hash = b.hash
            };
            b.history.Add(h);
            b.plys_since_search_start++;
            b.plys_since_game_start++;
            b.castling_rights = tmp_cr;

            // Update game variables (en passant, castling, 50 moves...)
            if (m.fm == 60) {
                b.castling_rights.Set(2, false);
                b.castling_rights.Set(3, false);
            }
            if (m.fm == 4) {
                b.castling_rights.Set(0, false);
                b.castling_rights.Set(1, false);
            }
            if (m.fm == 63 || m.to == 63)
                b.castling_rights.Set(3, false);
            if (m.fm == 56 || m.to == 56)
                b.castling_rights.Set(2, false);
            if (m.fm == 7 || m.to == 7)
                b.castling_rights.Set(1, false);
            if (m.fm == 0 || m.to == 0)
                b.castling_rights.Set(0, false);

            if ((m.move_type & Utils.PUSH_PAW) != 0)
                b.enpassant_idx = (sbyte)(m.to + (8 * b.side_to_move));
            else
                b.enpassant_idx = -1;

            if ((m.move_type & (Utils.PAWN_MVE + Utils.CAPTURE)) != 0)
                b.fifty_mve_rule = 0;
            else
                b.fifty_mve_rule++;

            if ((m.move_type & Utils.PROMOTE) != 0)
            {
                b.board[m.to] = (sbyte)(m.promotion * b.side_to_move);
            }
            else
            {
                b.board[m.to] = b.board[m.fm];
            }

            b.board[m.fm] = (sbyte)Utils.EMPTY;

            if ((m.move_type & Utils.ENPASS) != 0)
            {
                byte idx = (byte)(m.to + (8 * b.side_to_move));

                b.board[idx] = (sbyte)Utils.EMPTY;
            }

            if (m.fm == b.white_king_idx)
                b.white_king_idx = m.to;
            if (m.fm == b.black_king_idx)
                b.black_king_idx = m.to;

            b.hash = Utils.ComputeHash(b.board);
            b.side_to_move *= -1; // Let's change side and see how it goes

            // now that we've made the move, check if that's making us in check
            if (IsInCheck((sbyte)(b.side_to_move)) > 0){
                UnmakeLastMove();
                return Utils.IN_CHECK;
            }

            // change the side to play
            return Utils.LEGAL;
        }

        public void UnmakeLastMove()
        {
            b.side_to_move *= -1;
            b.plys_since_game_start--;
            b.plys_since_search_start--;
            History h = b.history.ElementAt(b.history.Count - 1);
            Move m = h.m;
            b.history.RemoveAt(b.history.Count - 1);

            b.castling_rights = h.castling_rights;
            b.enpassant_idx = h.enpassant_idx;
            b.fifty_mve_rule = h.fifty_mve_rule;
            b.hash = h.hash;

            if (m.to == b.white_king_idx)
                b.white_king_idx = m.fm;
            if (m.to == b.black_king_idx)
                b.black_king_idx = m.fm;

            if ((m.move_type & Utils.PROMOTE) != 0)
                b.board[m.fm] = (sbyte)(Utils.PAWN * b.side_to_move);
            else
                b.board[m.fm] = b.board[m.to];

            if (h.captured_piece == Utils.EMPTY)
                b.board[m.to] = (sbyte)Utils.EMPTY;
            else
                b.board[m.to] = h.captured_piece;

            if ((m.move_type & Utils.CASTLE) != 0)
            {
                byte rook_from_idx = 255;
                byte rook_to_idx = 255;

                if (b.side_to_move == Utils.WHITE)
                {
                    if ((m.move_type & Utils.KS_CASTLE) != 0)
                    {
                        rook_from_idx = 61;
                        rook_to_idx = 63;
                    } else if ((m.move_type & Utils.QS_CASTLE) != 0)
                    {
                        rook_from_idx = 59;
                        rook_to_idx = 56;
                    }
                }
                else
                {
                    if ((m.move_type & Utils.KS_CASTLE) != 0)
                    {
                        rook_from_idx = 5;
                        rook_to_idx = 7;
                    } else if ((m.move_type & Utils.QS_CASTLE) != 0)
                    {
                        rook_from_idx = 3;
                        rook_to_idx = 0;
                    }
                }

                b.board[rook_to_idx] = (sbyte)(Utils.ROOK * b.side_to_move);
                b.board[rook_from_idx] = (sbyte)Utils.EMPTY;
            }
            if ((m.move_type & Utils.ENPASS) != 0)
                b.board[m.to + (8 * b.side_to_move)] = (sbyte)(Utils.PAWN * b.side_to_move * -1);
        }
    }
}
