﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace processAI1
{
    class ArrayBoard
    {
        // TODO maybe add a flag for en passant but maybe not because platform should give the precise info about it
        public BitArray castling_rights; // by bit as | B Q side | B K side | W Q side | W K side |
        public bool i_am_white; // true = white black otherwise
        public bool side_set; // Has this already been done?
        public byte fifty_mve_rule;
        public byte white_king_idx;
        public byte black_king_idx;
        public sbyte side_to_move; // Who are we playing as rn?
        public sbyte enpassant_idx;
        public sbyte[] board;
        public UInt64 hash;
        public int plys_since_search_start;
        public int plys_since_game_start;
        public List<List<Move>> moves_list;
        public List<History> history;
        public Move[,] counter_h;
        public int[,,] history_h;

        public ArrayBoard()
        {
            board = new sbyte[64];
            i_am_white = false;
            side_set = false;
            side_to_move = Utils.WHITE;
            castling_rights = new BitArray(4, true);
            enpassant_idx = -1;
            fifty_mve_rule = 0;
            plys_since_game_start = 0;
            plys_since_search_start = 0;
            moves_list = new List<List<Move>>();
            history = new List<History>();
            history_h = new int[2, 64, 64];
            counter_h = new Move[64, 64];
            SetBoardDefault();
            hash = Utils.ComputeHash(board);
        }

        public String GameMovesToString()
        {
            StringBuilder sb = new StringBuilder();
            int i = 1;

            foreach (History h in history)
            {
                if (i % 2 == 1)
                    sb.Append("White : ");
                else
                    sb.Append("Black : ");

                sb.Append(h.m.ToString() + "\n");
            }
            return sb.ToString();
        }
        override public String ToString()
        {
            StringBuilder str = new StringBuilder();
            int idx = 0;
            foreach (sbyte sb in board)
            {
                if (idx % 8 == 0)
                    str.Append("\n" + (8 - Utils.LOOKUP_ROW[idx]) + "  ");
                if (sb > 0)
                    str.Append(" " + Utils.WHITE_PIECE[sb]);
                else
                    str.Append(" " + Utils.BLACK_PIECE[sb * -1]);
                idx++;
            }
            str.Append("\n\n    A B C D E F G H");
            str.Append("\n\n");
            if (castling_rights[1])
                str.Append("K");
            if (castling_rights[0])
                str.Append("Q");
            if (!(castling_rights[0] && castling_rights[1]))
                str.Append("-");
            if (castling_rights[3])
                str.Append("k");
            if (castling_rights[2])
                str.Append("q");
            if (!(castling_rights[3] && castling_rights[2]))
                str.Append("-");
            if (side_to_move == Utils.WHITE)
                str.Append(" w");
            else
                str.Append(" b");
            if (enpassant_idx != -1)
                str.Append(" " + enpassant_idx);
            else
                str.Append(" -");
            str.Append(" " + fifty_mve_rule + " " + (plys_since_game_start / 2));

            return str.ToString();
        }

        public void SetBoardSpe2()
        {
            for (int i = 0; i < 64; i++)
                this.board[i] = Utils.SPE2_GAME_START[i];
            white_king_idx = 60;
            black_king_idx = 4;
            enpassant_idx = -1;
            castling_rights = new BitArray(4, true);
            side_to_move = Utils.WHITE;
            fifty_mve_rule = 0;
            plys_since_game_start = 0;
            plys_since_search_start = 0;
            moves_list.Clear();
            history.Clear();
            hash = Utils.ComputeHash(board);
        }

        public void SetBoardSpe3()
        {
            for (int i = 0; i < 64; i++)
                this.board[i] = Utils.SPE3_GAME_START[i];
            white_king_idx = 24;
            black_king_idx = 39;
            enpassant_idx = -1;
            castling_rights = new BitArray(4, false);
            side_to_move = Utils.WHITE;
            fifty_mve_rule = 0;
            plys_since_game_start = 0;
            plys_since_search_start = 0;
            moves_list.Clear();
            history.Clear();
            hash = Utils.ComputeHash(board);
        }

        public void SetBoardSpe4()
        {
            for (int i = 0; i < 64; i++)
                this.board[i] = Utils.SPE4_GAME_START[i];
            white_king_idx = 62;
            black_king_idx = 4;
            enpassant_idx = -1;
            castling_rights = new BitArray(4, true);
            castling_rights.Set(2, false);
            castling_rights.Set(3, false);
            side_to_move = Utils.WHITE;
            fifty_mve_rule = 0;
            plys_since_game_start = 0;
            plys_since_search_start = 0;
            moves_list.Clear();
            history.Clear();
            hash = Utils.ComputeHash(board);
        }

        public void SetBoardSpe5()
        {
            for (int i = 0; i < 64; i++)
                this.board[i] = Utils.SPE5_GAME_START[i];
            white_king_idx = 60;
            black_king_idx = 5;
            enpassant_idx = -1;
            castling_rights = new BitArray(4, false);
            castling_rights.Set(2, true);
            castling_rights.Set(3, true);
            side_to_move = Utils.WHITE;
            fifty_mve_rule = 0;
            plys_since_game_start = 0;
            plys_since_search_start = 0;
            moves_list.Clear();
            history.Clear();
            hash = Utils.ComputeHash(board);
        }

        public void SetBoardSpe6()
        {
            for (int i = 0; i < 64; i++)
                this.board[i] = Utils.SPE6_GAME_START[i];
            white_king_idx = 62;
            black_king_idx = 6;
            enpassant_idx = -1;
            castling_rights = new BitArray(4, false);
            side_to_move = Utils.WHITE;
            fifty_mve_rule = 0;
            plys_since_game_start = 0;
            plys_since_search_start = 0;
            moves_list.Clear();
            history.Clear();
            hash = Utils.ComputeHash(board);
        }

        public void SetBoardDefault()
        {
            for (int i = 0; i < 64; i++)
                this.board[i] = Utils.DEF_GAME_START[i];
            white_king_idx = 60;
            black_king_idx = 4;
            enpassant_idx = -1;
            castling_rights = new BitArray(4, true);
            side_to_move = Utils.WHITE;
            fifty_mve_rule = 0;
            plys_since_game_start = 0;
            plys_since_search_start = 0;
            moves_list.Clear();
            history.Clear();
            hash = Utils.ComputeHash(board);
        }

        public void SetSide(bool side)
        {
            this.i_am_white = side;
            this.side_set = true;
        }

        private void SetSide(int[] tabVal)
        {
            if (!side_set)
            {
                for (int i = 0; i < 64; i++)
                {
                    if (tabVal[i] > 0)
                    {
                        if (i > 47)
                            i_am_white = true;
                        break;
                    }
                }
                side_set = true;
            }
        }

        public sbyte[] GenBoard(int[] tabVal)
        {
            sbyte[] res = new sbyte[64];

            if (tabVal.Length != 64)
                return res; // TODO log an error?

            SetSide(tabVal);

            for (int i = 0; i < 64; i++)
            {
                sbyte to_place = (sbyte) Utils.EMPTY;
                sbyte cur_color_mod = Utils.WHITE;

                int val = tabVal[i];
                res[i] = to_place; // init that place before finding out if it has relevant value

                if ((i_am_white && val < 0) || (!i_am_white && val > 0))
                    cur_color_mod = Utils.BLACK;

                switch (val)
                {
                    // Pawns placement
                    case Utils.P:
                    case -Utils.P:
                        to_place = (sbyte) (Utils.PAWN * cur_color_mod);
                        break;

                    // Rooks placement
                    case Utils.TG:
                    case Utils.TD:
                    case -Utils.TG:
                    case -Utils.TD:
                        to_place = (sbyte) (Utils.ROOK * cur_color_mod);
                        break;

                    // Knights placement
                    case Utils.CD:
                    case Utils.CG:
                    case -Utils.CD:
                    case -Utils.CG:
                        to_place = (sbyte) (Utils.KNIGHT * cur_color_mod);
                        break;


                    // Bishops placement
                    case Utils.F:
                    case -Utils.F:
                        to_place = (sbyte) (Utils.BISHOP * cur_color_mod);
                        break;

                    // Queen placement
                    case Utils.D:
                    case -Utils.D:
                        to_place = (sbyte) (Utils.QUEEN * cur_color_mod);
                        break;

                    // King placement
                    case Utils.R:
                    case -Utils.R:
                        to_place = (sbyte) (Utils.KING * cur_color_mod);
                        if (val == Utils.R)
                            white_king_idx = (byte)i;
                        else
                            black_king_idx = (byte)i;
                        break;

                    default:
                        break;
                }

                if (to_place != Utils.EMPTY)
                {
                    res[i] = to_place;
                }
            }
            return res;
        }
    }
}