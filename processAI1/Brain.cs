﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace processAI1
{
    class Brain
    {
        private ArrayBoard b;
        public MoveGen mg;
        private int cutoffs;
        public Stopwatch sw;
        private const long timer = 220;
        private const long time_limit = 230;

        public Brain(ArrayBoard b)
        {
            this.b = b;
            this.mg = new MoveGen(b);
            cutoffs = 0;
            sw = new Stopwatch();
        }

        public void Test()
        {
            long computation_time = 0;
            while (true)
            {
                string str = "";
                if (b.side_to_move == Utils.WHITE)
                    str = "White";
                else
                    str = "Black";

                RootMove rm = IterativeDepthFindBestMoveWithDepth(4);
                computation_time = sw.ElapsedMilliseconds;

                if (rm.m.to != rm.m.fm && b.fifty_mve_rule < 100)
                {

                    Console.WriteLine(str + " found their best move as " + rm.m.ToString() + " with score : "
                        + rm.score + " and " + cutoffs + " cutoffs in : " + computation_time + " ms");
                    if (computation_time > time_limit)
                        Console.ReadKey();
                    mg.MakeMove(rm.m);
                    Console.WriteLine(b.ToString() + "\n");
                }
                else
                {
                    Console.WriteLine(str + " couldn't find a move : \n" + b.ToString());
                    break;
                }
            }
        }

        public RootMove IterativeDepthFindBestMoveWithDepth(int depth)
        {
            RootMove best = new RootMove();
            sw.Restart();

            for (int i = 1; i < depth + 1; i++) {
                if (sw.ElapsedMilliseconds > timer)
                    break;
                best = FindBestMoveWithDepth(i);
            }

            sw.Stop();
            best.time = sw.ElapsedMilliseconds;

            return best;
        }

        public RootMove FindBestMoveWithDepth(int depth)
        {
            int alpha = Utils.SEARCH_WINDOW_L;
            int beta = Utils.SEARCH_WINDOW_H;
            this.b.plys_since_search_start = 0;
            cutoffs = 0;
            RootMove best = new RootMove(new Move(), int.MinValue);

            SortedSet<Move> moves = new SortedSet<Move>();
            mg.GenAllLegalMoves(moves);


            foreach (Move m in moves)
            {
                RootMove rm = new RootMove(m, 0);
                if (sw.ElapsedMilliseconds > timer)
                    break;
                if (mg.MakeMove(rm.m) == Utils.LEGAL)
                {
                    rm.SetScore(-PVS(alpha, beta, depth));
                    if (rm.score > best.score)
                        best = rm;

                    mg.UnmakeLastMove();
                }
                else
                    continue;
            }
            return best;
        }

        public int EvalCurrentPosition()
        {
            sbyte stm = b.side_to_move;
            int result = 0;
            byte attackers_nb = 0;

            // TODO handle late game king values
            for (int i = 0; i < 64; i++)
            {
                sbyte curr_piece = (sbyte)(b.board[i] * stm);
                if (curr_piece < 0 || curr_piece == Utils.EMPTY)
                    continue;
                if (stm == Utils.WHITE)
                    result += Utils.PIECES_EVAL_W[curr_piece][i];
                else
                    result += Utils.PIECES_EVAL_B[curr_piece][i];
                result += Utils.PIECE_VALUE[curr_piece];
            }

            if ((attackers_nb = mg.IsInCheck((sbyte)(stm))) > 0)
                result += attackers_nb * Utils.PIECE_VALUE[Utils.KING];

            return result;
        }

        private int PVS(int alpha, int beta, int depth)
        {
            if (sw.ElapsedMilliseconds > timer)
                return alpha;

            bool first = true;
            bool no_more_moves = true;
            int score = 0;

            if (depth == 0)
                return EvalCurrentPosition(); // TODO Quiescence search

            // Using transposition table
            HashNode hn = new HashNode();
            Move best = new Move();

            if (Utils.IsHit(b.hash, ref hn))
            {
                if (hn.depth >= depth)
                {
                    switch (hn.flag)
                    {
                        case Utils.LOWER_BOUND_F:
                            if (alpha < hn.score)
                                alpha = hn.score;
                            break;
                        case Utils.UPPER_BOUND_F:
                            if (beta > hn.score)
                                beta = hn.score;
                            break;
                        case Utils.EXACT_SCORE_F:
                            return hn.score;
                    }
                    if (alpha >= beta)
                        return hn.score;
                }
                best = hn.best_mve;
                best.score = Utils.SEARCH_WINDOW_L;
                if (best.fm != best.to && b.board[best.fm] * b.side_to_move > 0
                    && (b.board[best.to] == Utils.EMPTY || b.board[best.to] * b.side_to_move < 0))
                    best.score = hn.best_mve.score;
            }

            SortedSet<Move> moves = new SortedSet<Move>();
            mg.GenAllLegalMoves(moves);

            if (best.score != Utils.SEARCH_WINDOW_L)
                moves.Add(best);

            foreach (Move m in moves)
            {
                if (sw.ElapsedMilliseconds > timer)
                    return alpha;

                if (mg.MakeMove(m) == Utils.LEGAL)
                {
                    no_more_moves = false;

                    if (first)
                    {
                        first = false;
                        score = -PVS(-beta, -alpha, depth - 1);
                    }
                    else
                    {
                        score = -PVS(-alpha - 1, -alpha, depth - 1);
                        if (score > alpha)
                            score = -PVS(-beta, -score, depth - 1);
                    }

                    mg.UnmakeLastMove();

                    if (score >= beta)
                    {
                        cutoffs++;
                        if ((m.move_type & Utils.CAPTURE) == 0)
                        {
                            if (b.side_to_move == Utils.WHITE)
                                b.history_h[0, m.fm, m.to] += depth * depth;
                            else
                                b.history_h[1, m.fm, m.to] += depth * depth;

                            b.counter_h[b.history.Last().m.fm, b.history.Last().m.to] = m;
                        }
                        Utils.InsertEntry(b.hash, m, depth, score, Utils.UPPER_BOUND_F);
                        return beta;
                    }
                    if (score < alpha)
                    {
                        Utils.InsertEntry(b.hash, new Move(), depth, score, Utils.LOWER_BOUND_F);
                    }
                    if (score > alpha)
                    {
                        Utils.InsertEntry(b.hash, new Move(), depth, score, Utils.EXACT_SCORE_F);
                        alpha = score;
                    }
                }
            }

            if (no_more_moves)
            {
                int tmp = 0;
                if ((tmp = mg.IsInCheck((sbyte)(b.side_to_move * -1))) > 0)
                    return tmp * Utils.SEARCH_WINDOW_L;
                return 0;
            }

            if (b.fifty_mve_rule >= 100)
                return 0;

            return alpha;
        }
    }
}
