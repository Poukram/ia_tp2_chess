﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace processAI1
{
    class Perft
    {
        private ArrayBoard b;
        private MoveGen mg;
        
        public Perft(ArrayBoard b)
        {
            this.b = b;
            mg = new MoveGen(this.b);
        }

        public Perft()
        {
            b = new ArrayBoard();
            b.SetSide(true); // we are white
            mg = new MoveGen(this.b);
        }

        public void RunConfigurationAtDepth(int pos_idx, int max_depth)
        {
            switch (pos_idx)
            {
                case 1:
                    b.SetBoardDefault();
                    break;
                case 2:
                    b.SetBoardSpe2();
                    break;
                case 3:
                    b.SetBoardSpe3();
                    break;
                case 4:
                    b.SetBoardSpe4();
                    break;
                case 5:
                    b.SetBoardSpe5();
                    break;
                case 6:
                    b.SetBoardSpe6();
                    break;
                default:
                    Console.WriteLine("Can't find that position, sorry");
                    return;
            }

            Run(max_depth, "Configuration (#" + pos_idx + " chessprogramming perft)");
        }

        private void Run(int max_depth, String game_type)
        {
            PerftInfo res = RunAtDepth(max_depth);
            Console.WriteLine("Perft({1}) for {0} :\n{2}", game_type, max_depth, res.ToString());
        }

        
        // TODO set advanced flags only at the leaf
        private PerftInfo RunAtDepth(int depth)
        {
            PerftInfo pi = new PerftInfo();

            if (depth == 0)
            {
                bool checkmate = true;
                SortedSet<Move> test_moves = new SortedSet<Move>();
                mg.GenAllLegalMoves(test_moves);

                foreach (Move m in test_moves) {
                    if (mg.MakeMove(m) == Utils.LEGAL)
                    {
                        checkmate = false;
                        mg.UnmakeLastMove();
                        break;
                    }
                }
                if (checkmate)
                    pi.checkma++;
                pi.nodes_computed++;
                return pi;
            }

            SortedSet<Move> moves = new SortedSet<Move>();
            mg.GenAllLegalMoves(moves);

            foreach (Move m in moves)
            {
                if (mg.MakeMove(m) == Utils.LEGAL)
                {
                    pi += RunAtDepth(depth - 1);

                    // Perft details given only for leaf nodes
                    if (depth - 1 == 0)
                    {
                        if ((m.move_type & Utils.CAPTURE) != 0)
                            pi.capture++;
                        if ((m.move_type & Utils.ENPASS) != 0)
                            pi.enpassa++;
                        if ((m.move_type & Utils.CASTLE) != 0)
                            pi.castles++;
                        if ((m.move_type & Utils.PROMOTE) != 0)
                            pi.promoti++;
                        if (mg.IsInCheck((sbyte)(-1 * b.side_to_move)) > 0)
                            pi.checks++;
                    }
                    mg.UnmakeLastMove();
                }
            }
            return pi;
        }
    }
}
