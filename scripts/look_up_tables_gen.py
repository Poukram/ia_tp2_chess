res = [] # 8x8 to 0x88 lookup
inv = [] # 0x88 to 8x8 lookup
col = [] # column number of an 8x8 square
row = [] # row number of an 8x8 square
sq_rel_idx = [[-1 for i in range(64)] for j in range(64)]# a 64 bye 64 look up table to get the index in the square relation lookup table (computing in on the fly costs too much)
square_rel = [255]*240; # a table of 240 elements using the diff between two 0x88 square to map Chebyshev distance and direction of attack as From - To lookup
piece_eq = {"p" : -1, "n" : -2, "b" : -3, "r" : -4, "q" : -5, "k" : -6,
            "P" : 1, "N" : 2, "B" : 3, "R" : 4, "Q" : 5, "K" : 6};
def cs_pretty_print(array):
        tmp = "";
        tmp = str(array);
        tmp = tmp.replace('[', '{');
        tmp = tmp.replace(']', '}');
        tmp = tmp.replace("},", "},\n");
        print(tmp)

def fen_str_to_cs_array(fen_str):
        result = [7]*64; # 7 <=> Empty square in our engine
        i = 0;
        for c in fen_str:
                if c.isdecimal():
                        i += int(c);
                elif c.isalpha():
                        result[i] = piece_eq.get(c);
                        i += 1;
                if i > 63:
                        break;
        return result;
                           

def chebychev(sq1, sq2, base):
        c1 = int(sq1 / base);
        r1 = int(sq1 % base);
        c2 = int(sq2 / base);
        r2 = int(sq2 % base);
        return max(abs(c2 - c1), abs(r2 - r1));

# takes two indexes on the 8x8 board and gives the relation between the two using the 0x88 squares relations lookup table
def test_sq_relations(from_idx, to_idx):
        k = 0x77 + res[to_idx] - res[from_idx];
        return square_rel[k];

# Returns a direction in the 8x8 board from a direction in the 0x88 board
def dir_get(from_idx, to_idx, dist):
        if dist == 0:
                dist = 1;

        diff = (to_idx - from_idx)
        res = diff / dist;
        direction = 16;
        
        common = {
                -16:0,  #N
                16:1,   #S
                1:2,    #E
                -1:3,   #W
                -15:4,  #NE
                -17:5,  #NW
                17:6,   #SE
                15:7    #SW
                };
        knight = {
                -31:8,  #NNE
                -33:9,  #NNW
                33:10,  #SSE
                31:11,  #SSW
                -14:12, #EEN
                18:13,  #EES
                -18:14, #WWN
                14:15   #WWS
                };

        if res.is_integer():
                direction = common.get(int(res), 16);
        if direction == 16:
                direction = knight.get(diff, 16);
        return direction;

for i in range(64):
	res.append(i + (i & ~7))

for i in range(8*16):
        if (i in res):
                inv.append((i + (i & 7)) >> 1)
        else:
                inv.append(-1);

for i in range (64):
        row.append(i >> 3);
        col.append(i & 7);

for i in res:
        for j in res:
                k = 0x77 + i - j;
                if (square_rel[k] ==255 or True):
                        dist = chebychev(i, j, 16);
                        direction = dir_get(j, i, dist);
                        square_rel[k] = ((dist & 0x0F) | (direction << 3));

for f in range(64):
        for t in range(64):
                sq_rel_idx[f][t] = 0x77 + res[t] - res[f] # access is from to    
        
cs_pretty_print(res)
cs_pretty_print(inv)
cs_pretty_print(col)
cs_pretty_print(row)
cs_pretty_print(square_rel)
cs_pretty_print(sq_rel_idx)


# cf http://chessprogramming.wikispaces.com/Perft+Results for positions details
print("\nKiwipete starting position (w KQkq -)")
cs_pretty_print(fen_str_to_cs_array("r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -")) # Kiwipete

print("\nThird starting position (w - -)")
cs_pretty_print(fen_str_to_cs_array("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -")) # Position 3

print("\nFourth starting position (w kq -)")
cs_pretty_print(fen_str_to_cs_array("r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1")) # Position 4

print("\nFifth starting position (w KQ -)")
cs_pretty_print(fen_str_to_cs_array("rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8")) # Position 5

print("\nSixth starting position (w - -)")
cs_pretty_print(fen_str_to_cs_array("r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - -")) # Position 6

# Testing the lookup table
a4b7 = test_sq_relations(9, 32);
d5e3 = test_sq_relations(27, 44);
a8g8 = test_sq_relations(0, 6);
a1e8 = test_sq_relations(56, 4);
print("A4 to B7 gives relation : %3d -> "%(a4b7,) + "{:#010b}".format(a4b7));
print("D5 to E3 gives relation : %3d -> "%(d5e3,) + "{:#010b}".format(d5e3));
print("A8 to G8 gives relation : %3d -> "%(a8g8,) + "{:#010b}".format(a8g8));
print("A1 to E8 gives relation : %3d -> "%(a1e8,) + "{:#010b}".format(a1e8));

